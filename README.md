# This is just a simple and light blog written in Symfony 3.1 and Bootstrap 3.3#

## My Environment ##
**Operating System:** MacOSX

**IDE:** PHPStorm

**Virtualization Environment:** Homestead (Vagrant Wrapper)

## How to Install? ##

1 - Clone this repo into your destination directory.

```
#!terminal

git clone https://aelaraby93@bitbucket.org/aelaraby93/symfony-blog.git <dist_dir>
```


2 - Go to your destination directory

```
#!terminal

cd <dist_dir>
```


3 - Install Dependencies

```
#!terminal

composer install --optimize-autoloader
```

4 - Create database

```
#!terminal

php bin/console doctrine:database:create
```

5 - Create the database schema

```
#!terminal

php bin/console doctrine:schema:update -f
```

6 - Install Fixtures (Currently there is only one fixture for Admin User)

```
#!terminal

php bin/console doctrine:fixtures:load
```

7 - Dump the assetics

```
#!terminal

php bin/console assetic:dump
```

## Admin User: ##
Username: admin (OR) Email: shadowsolutions93@gmail.com

Password: admin

## External Bundles Used: ##
Only Bootstrap and JQuery

## Wakatime Log for this project ##
![Screen Shot 2016-10-22 at 7.27.21 PM.png](https://bitbucket.org/repo/6bdozp/images/509745016-Screen%20Shot%202016-10-22%20at%207.27.21%20PM.png)