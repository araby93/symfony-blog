<?php

namespace AE\BlogBundle\Listener;

use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

/**
 * Class BeforeRequestPublishedFilter
 * @package AE\BlogBundle\Listener
 */
class BeforeRequestPublishedFilter
{
    protected $em;
    protected $authorizationChecker;
    protected $reader;

    /**
     * BeforeRequestPublishedFilter constructor.
     *
     * @param ObjectManager                 $em
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param Reader                        $reader
     */
    public function __construct(ObjectManager $em, AuthorizationCheckerInterface $authorizationChecker, Reader $reader)
    {
        $this->em              = $em;
        $this->authorizationChecker    = $authorizationChecker;
        $this->reader          = $reader;
    }

    /**
     * OnKernelRequest Event Handler
     */
    public function onKernelRequest()
    {
        try {
            if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                $filter = $this->em->getFilters()->enable('post_published');
                $filter->setParameter('isPublished', true);
                $filter->setAnnotationReader($this->reader);
            }
        } catch (AuthenticationCredentialsNotFoundException $e) {
        }
    }
}
