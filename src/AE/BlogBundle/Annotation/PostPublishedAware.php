<?php

namespace AE\BlogBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class PostPublishedAware
 * @package AE\BlogBundle\Annotation
 *
 * @Annotation
 * @Annotation\Target("CLASS")
 */
class PostPublishedAware
{
    public $isPublishedFieldName;
}
