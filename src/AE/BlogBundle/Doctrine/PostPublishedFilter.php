<?php

namespace AE\BlogBundle\Doctrine;

use AE\BlogBundle\Annotation\PostPublishedAware;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Filtering the posts based on the publishing status for non-admin users.
 *
 * Class PostPublishedFilter
 * @package AE\BlogBundle\Doctrine
 */
class PostPublishedFilter extends SQLFilter
{
    protected $reader;

    /**
     * Gets the SQL query part to add to a query.
     *
     * @param ClassMetaData $targetEntity
     * @param string        $targetTableAlias
     *
     * @return string The constraint SQL if there is available, empty string otherwise.
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (empty($this->reader)) {
            return '';
        }

        // The Doctrine filter is called for any query on any entity
        // Check if the current entity is "user aware" (marked with an annotation)
        $postPublishedAware = $this->reader->getClassAnnotation(
            $targetEntity->getReflectionClass(),
            PostPublishedAware::class
        );

        if (!$postPublishedAware) {
            return '';
        }

        $fieldName = $postPublishedAware->isPublishedFieldName;

        try {
            // Don't worry, getParameter automatically quotes parameters
            $isPublished = $this->getParameter('isPublished');
        } catch (\InvalidArgumentException $e) {
            // No user id has been defined
            return '';
        }

        if (empty($fieldName) || empty($isPublished)) {
            return '';
        }

        return sprintf('%s.%s = %s', $targetTableAlias, $fieldName, $isPublished);
    }

    /**
     * Just to inject the annotation reader
     *
     * @param Reader $reader
     */
    public function setAnnotationReader(Reader $reader)
    {
        $this->reader = $reader;
    }
}
