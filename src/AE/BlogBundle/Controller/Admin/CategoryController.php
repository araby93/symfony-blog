<?php

namespace AE\BlogBundle\Controller\Admin;

use AE\BlogBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Category controller.
 *
 * @Route("/admin/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @return Response
     *
     * @Route("/", name="admin_category_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('AEBlogBundle:Category')->findAll();

        return $this->render('AEBlogBundle:Admin:Category/index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new Category entity.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/new", name="admin_category_new")
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm('AE\BlogBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                sprintf('Category "%s" has been created successfully.', $category->getName())
            );

            return $this->redirectToRoute('admin_category_show', array('id' => $category->getId()));
        }

        return $this->render('AEBlogBundle:Admin:Category/new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Category entity.
     *
     * @param Category $category
     *
     * @return Response
     *
     * @Route("/{id}/show", name="admin_category_show", requirements = { "id" = "\d+" })
     */
    public function showAction(Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);

        return $this->render('AEBlogBundle:Admin:Category/show.html.twig', array(
            'category' => $category,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     * @param Request  $request
     * @param Category $category
     *
     * @return Response
     * @Route("/{id}/edit", name="admin_category_edit", requirements = { "id" = "\d+" })
     */
    public function editAction(Request $request, Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm('AE\BlogBundle\Form\CategoryType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                sprintf('Category "%s" has been updated successfully.', $category->getName())
            );

            return $this->redirectToRoute('admin_category_index');
        }

        return $this->render('AEBlogBundle:Admin:Category/    edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Category entity.
     *
     * @Route("/{id}/delete", name="admin_category_delete", requirements = { "id" = "\d+" })
     * @param Request  $request
     * @param Category $category
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
            $this->addFlash(
                'success',
                sprintf('Category "%s" has been deleted successfully.', $category->getName())
            );
        }

        return $this->redirectToRoute('admin_category_index');
    }

    /**
     * Creates a form to delete a Category entity.
     *
     * @param Category $category The Category entity
     *
     * @return Form The form
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
