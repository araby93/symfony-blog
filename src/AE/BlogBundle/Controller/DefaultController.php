<?php

namespace AE\BlogBundle\Controller;

use AE\BlogBundle\Entity\Category;
use AE\BlogBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController.
 *
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * Lists all Published Posts to visitors.
     *
     * @param Category $category
     *
     * @return Response
     *
     * @Route("/", name="ae_blog_homepage")
     * @Route("/category/{id}", name="ae_blog_homepage_category", requirements={"id" = "\d+"})
     */
    public function indexAction(Category $category = null)
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('AEBlogBundle:Category')->findAll();

        if ($category !== null) {
            $posts = $category->getPosts();
        } else {
            $posts = $em->getRepository('AEBlogBundle:Post')->findBy(
                [],
                ['id' => 'DESC']
            );
        }

        return $this->render('AEBlogBundle:Default:index.html.twig', array(
            'posts' => $posts,
            'categories' => $categories,
        ));
    }

    /**
     * Finds and displays a Post entity.
     *
     * @param Post $post
     *
     * @return Response
     */
    public function showAction(Post $post)
    {
        return $this->render('AEBlogBundle:Post:show.html.twig', array(
            'post' => $post,
        ));
    }
}
