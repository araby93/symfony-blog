<?php

namespace AE\BlogBundle\Controller;

use AE\BlogBundle\Entity\Post;
use AE\BlogBundle\Entity\PostComment;
use AE\BlogBundle\Form\PostCommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Post controller.
 *
 * @Route("/post")
 */
class PostController extends Controller
{
    /**
     * Finds and displays a Post entity for visitors.
     *
     * @Route("/{id}/show", name="ae_post_show", requirements = { "id" = "\d+" })
     * @param Request $request
     * @param Post    $post
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, Post $post)
    {
        $categories = $this->getDoctrine()->getRepository('AEBlogBundle:Category')->findAll();

        $comment = new PostComment();
        $comment->setPost($post);
        $commentForm = $this->createForm(PostCommentType::class, $comment);
        $commentForm->handleRequest($request);

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $this->addFlash(
                'success',
                'Your comment has been added successfully.'
            );

            return $this->redirectToRoute('ae_post_show', array('id' => $post->getId()));
        }

        return $this->render('AEBlogBundle:Post:show.html.twig', array(
            'post' => $post,
            'categories' => $categories,
            'comment_form' => $commentForm->createView(),
        ));
    }
}
