$('button.delete-btn').on('click', function (e) {
    e.preventDefault();
    var actionBtnSelector = '#modal-confirmation-primary-btn';

    var _form = $(this).closest('form');
    var _modal = $('#modal-confirmation');
    _modal.find('.modal-body').html(
        $(this).data('message')
    );

    _modal.find(actionBtnSelector).html(
        $(this).data('action').charAt(0).toUpperCase() + $(this).data('action').slice(1).toLowerCase()
    )
        .addClass($(this).data('actionClass'));

    _modal.modal({backdrop: 'static', keyboard: false})
        .one('click', actionBtnSelector, function () {
            _form.trigger('submit');
        });
});