<?php
/**
 * User: araby
 * Date: 10/22/16
 * Time: 6:41 PM
 */

namespace AE\UserBundle\DataFixtures\ORM;


use AE\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

/**
 * Class LoadUserData
 * @package AE\UserBundle\DataFixtures\ORM
 */
class LoadUserData implements FixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $encoder = new BCryptPasswordEncoder(12);
        $userAdmin = new User();
        $userAdmin->setDisplayName('Ahmed El-Araby');
        $userAdmin->setEmail('shadowsolutions93@gmail.com');
        $userAdmin->setIsActive(true);
        $userAdmin->setRoles(['ROLE_ADMIN']);
        $userAdmin->setUsername('admin');
        $userAdmin->setPassword($encoder->encodePassword('admin', null));

        $manager->persist($userAdmin);
        $manager->flush();
    }
}